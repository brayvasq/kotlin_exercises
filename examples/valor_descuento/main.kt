package valor_descuento

import java.util.*

/**
 * Created by brayan on 15/04/2017.
 */
fun main(args : Array<String>){

    var scan = Scanner(System.`in`)

    println("Digite el valor pagado")
    var valor = scan.nextDouble()
    println("Digite la tarifa de descuento")
    var tarifa = scan.nextDouble()
    println("El valor total a pagar es : \n ${descuento(valor,tarifa)}")
}

fun descuento(pago:Double=0.0,tarifa:Double=0.0) = pago*(tarifa/100)