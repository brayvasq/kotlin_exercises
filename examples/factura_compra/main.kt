package factura_compra

import java.util.*

/**
 * Created by brayan on 16/04/2017.
 */
fun main(args: Array<String>) {
    var scan = Scanner(System.`in`)

    println("Digite el nombre del producto que se va a comprar")
    var producto = scan.next()
    println("Digite el precio del producto ")
    var precio = scan.nextDouble()
    println("Digite la cantidad de unidades a comprar ")
    var cantidad = scan.nextInt()
    factura(producto,precio,cantidad)
}

fun factura(producto:String="",precio:Double=0.0,cantidad:Int=0){
    var precioFinal : Double = precio + (precio * 0.19)

    println("Compra de  : ${producto}")
    println("Precio por unidad : ${precio}")
    println("IVA : 19%")
    println("Precio por unidad + IVA : ${precioFinal}")

    if(precioFinal > 13000){
        precioFinal = precioFinal - (precioFinal * 0.05)
        println("Precio por unidad - descuento 5% : ${precioFinal}")
    }

    println("Cantidad Total a pagar : ${precioFinal * cantidad}")

}