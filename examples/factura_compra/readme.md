### Factura Compra

Se trata de escribir el algoritmo que permita emitir la factura correspondiente a
una compra de un articulo determinado, del que se adquieren una o varias unidades.
El IVA es del 19% y si el precio bruto ( precio venta mas IVA) es mayor de $13000 se
debe realizar un descuento del 5%.