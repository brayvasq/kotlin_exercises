package triangulo_rectangulo

import java.util.*

/**
 * Created by brayan on 15/04/2017.
 */
fun main(args: Array<String>) {
    var scan = Scanner(System.`in`)

    println("Digite el valor de la base del triangulo : ")
    var base = scan.nextDouble()
    println("Digite el valor de la altura del triangulo : ")
    var altura = scan.nextDouble()

    println("El area del triangulo es : \n${area(base,altura)}")
    println("El perimetro del triangulo es : \n${perimetro(base,altura)}")
}

fun area(b:Double=0.0,h:Double=0.0):Double = b + h + (Math.sqrt(Math.pow(b,2.0)+Math.pow(h,2.0)))

fun perimetro(b:Double=0.0,h:Double=0.0):Double = (b * h) / 2