package calcular_salario

import java.util.*

/**
 * Created by brayan on 15/04/2017.
 */
fun main(args: Array<String>) {
    var scan = Scanner(System.`in`)

    println("Digite la tarifa por hora : ")
    var tarifa = scan.nextDouble()
    println("Digite el numero de horas trabajadas : ")
    var horas = scan.nextDouble()

    println("El sueldo a pagar es : ${calcular(horas,tarifa)}")

}

fun calcular(horas:Double=0.0,tarifa:Double=0.0) : Double{
    var horasComunes : Double = horas
    var horasExtras  : Double = 0.0

    if (horas > 40){
        horasExtras = horas - 40;
        horasComunes = horas - horasExtras
    }

    return (horasComunes * tarifa) + (horasExtras * (tarifa + (tarifa * 0.5)))
}


