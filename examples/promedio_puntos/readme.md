### Promedio puntos

Se tiene un grupo de 3 personas, para cada una de las cuales se ha elaborado una tarjeta
de registro indicando el sexo y los puntos obtenidos en un examen.  Se desea conocer
con base en los promedios de los puntos obtenidos, cual sexo tuvo mejor desempeño.