package hipotenusa

import java.util.*

/**
 * Created by brayan on 15/04/2017.
 */

fun main(args: Array<String>){
    var scan = Scanner(System.`in`)

    println("Ingrese valor del cateto a : ")
    var a = scan.nextDouble()
    println("Ingrese valor del cateto b : ")
    var b = scan.nextDouble()

    println("La hipotenusa tiene un valor de : \n"+ hipotenusa(a,b))
}

fun hipotenusa(a:Double = 0.0,b:Double = 0.0): Double {
    return Math.sqrt((Math.pow(a,2.0)+Math.pow(b,2.0)))
}
