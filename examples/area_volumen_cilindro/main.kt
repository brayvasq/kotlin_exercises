package area_volumen_cilindro

import java.util.*

/**
 * Created by brayan on 15/04/2017.
 */

fun main(args: Array<String>){

    var scan = Scanner(System.`in`)

    println("Digite el Radio R : ")
    var r = scan.nextDouble()
    println("Digite la Altura H : ")
    var h = scan.nextDouble()

    println("El Area es : \n"+ area(r,h))
    println("El Volumen es : \n"+ volumen(r,h))
}

fun area(R:Double = 0.0,H:Double = 0.0) : Double{
    return (2 * Math.PI * R * H)
}

fun volumen(R:Double = 0.0,H:Double = 0.0) : Double{
    return (Math.PI * Math.pow(R,2.0) * H)
}