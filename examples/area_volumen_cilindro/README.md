### Area y volumen de un cilindro

Desarrollar un algoritmo que permita determinar el área y volumen de un cilindro
dado su radio (R) y altura (H).

> A= 2 *pi *R*H

> Vol= pi * R^2 * H