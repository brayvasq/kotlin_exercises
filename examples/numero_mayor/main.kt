package numero_mayor

import java.util.*

/**
 * Created by brayan on 15/04/2017.
 */
fun main(args : Array<String>){

    var scan = Scanner(System.`in`)

    println("Ingrese numero a : ")
    var a = scan.nextInt()
    println("Ingrese numero b : ")
    var b = scan.nextInt()

    println(mayor(a,b))
}

fun mayor(num1:Int = 0,num2:Int = 0):String = if(num1>num2) "El numero mayor es ${num1} \n orden: ${num2} ${num1}" else "El numero mayor es ${num2} \n orden: ${num1} ${num2}"