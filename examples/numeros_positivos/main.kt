package numeros_positivos

import java.util.*

/**
 * Created by brayan on 16/04/2017.
 */
fun main(args: Array<String>) {
    var scan = Scanner(System.`in`)
    var cad : String = ""
    var numero : Int
    for (i:Int in 0..4){
        println("Digite un numero : ")
        numero = scan.nextInt()
        if (numero>0) cad += " "+numero
    }

    println("Los numeros negativos son : \n ${cad}")
}