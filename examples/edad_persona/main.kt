package edad_persona

import java.util.*

/**
 * Created by brayan on 16/04/2017.
 */
fun main(args: Array<String>) {
    var scan = Scanner(System.`in`)
    println("Digite su nombre : ")
    var nombre = scan.next()
    println("Digite su edad : ")
    var edad = scan.nextInt()
    println("Digite su sexo: \nM=>masculino \nF=>Femenino \nO=>Otro - indefinido")
    var sexo = scan.next()
    println("Digite su estado civil: \nS=>soltero/a \nC=>casado/a \nV=>viudo/a")
    var civil = scan.next()
    verificarEdad(nombre,edad,sexo,civil)
}

fun verificarEdad(nombre:String="",edad:Int=0,sexo:String="M",civil:String=""){

    if(edad>30 && sexo=="M" && civil == "S"){
        println("${nombre} es un hombre soltero mayor de 30 años")
    }else if(edad<50 && sexo=="F" && civil=="V"){
        println("${nombre} es una mujer viuda menor de 50 años")
    }

}
