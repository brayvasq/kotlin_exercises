### Edad persona

Desarrolle un algoritmo que lea el nombre, la edad, el sexo, el estado civil
de cualquier persona e imprima el nombre de la persona si corresponde a un hombre
soltero, mayor de 30 años o a una mujer viuda menor de 50 años.