package calcular_nota

import java.util.*

/**
 * Created by brayan on 15/04/2017.
 */

fun main(args : Array<String>){
    var scan = Scanner(System.`in`)

    println("Ingrese su nota de primer corte : ")
    var nota1 = scan.nextDouble()
    println("Ingrese su nota de segundo corte : ")
    var nota2 = scan.nextDouble()
    println("Ingrese su nota de tercer corte : ")
    var nota3 = scan.nextDouble()

    println("Su nota final es : \n"+ calcular(nota1,nota2,nota3))

}

fun calcular(nota1:Double = 0.0, nota2:Double = 0.0,nota3:Double = 0.0):Double{
    return (nota1 * 0.35) + (nota2 * 0.35) + (nota3 * 0.35)
}