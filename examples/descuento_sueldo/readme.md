### Descuento sueldo

A un trabajador le descuentan de su sueldo el 10% si su sueldo es menor o igual a
1000, por encima de 1000 y hasta 2000 el 5% del adicional, y por encima de 2000 el
3% del adicional. Calcular el descuento y sueldo neto que recibe el trabajador dado su
sueldo.