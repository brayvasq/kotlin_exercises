package descuento_sueldo

import java.util.*

/**
 * Created by brayan on 16/04/2017.
 */
fun main(args: Array<String>) {
    var scan = Scanner(System.`in`)

    println("Digite su sueldo : ")
    var sueldo = scan.nextDouble()
    descuento(sueldo)
}

fun  descuento(sueldo:Double = 0.0){
    var desc:Double = 0.0
    var pago:Double = 0.0
    if (sueldo<=1000){
        desc = 0.10
        pago = sueldo - (sueldo * desc)
    }else if(sueldo>1000 && sueldo<=2000){
        desc = 0.05
        pago = sueldo - (sueldo * desc)
    }else if(sueldo>2000){
        desc = 0.03
        pago = sueldo - (sueldo * desc)
    }

    println("Su sueldo es : \n${sueldo}")
    println("El descuento a realizar es : \n${desc}")
    println("Su sueldo final es : \n${pago}")
}