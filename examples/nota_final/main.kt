package nota_final

import java.util.*

/**
 * Created by brayan on 15/04/2017.
 */
fun main(args: Array<String>){
    var scan = Scanner(System.`in`)

    for(i:Int in 0..1){
        println("Digite su nombre : ")
        var name = scan.next()
        println("Digite su nota : ")
        var nota = scan.nextDouble()
        println(aprobo(name,nota))
    }

}

fun aprobo(name:String="",nota:Double=0.0):String = if(nota>=3.0) "${name} aprobó la materia con nota ${nota}" else "${name} No aprobó la materia con nota ${nota}"