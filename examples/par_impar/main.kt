package par_impar

import java.util.*

/**
 * Created by brayan on 15/04/2017.
 */
fun main(args: Array<String>){
    var scan = Scanner(System.`in`)

    println("Ingrese el numero a evaluar : ")
    var num = scan.nextInt()
    println(par_impar(num))
}

fun par_impar(num:Int = 0):String = if ((num%2) == 0) "El numero es par" else "El numero es impar"