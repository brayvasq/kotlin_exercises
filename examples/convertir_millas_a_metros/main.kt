package convertir_millas_a_metros

import java.util.*

/**
 * Created by brayan on 15/04/2017.
 */

fun main(args : Array<String>){
    var scan = Scanner(System.`in`)
    println("Digite el valor en millas")
    var millas = scan.nextDouble()
    println("El valor en metros es : \n"+ convertir(millas))
}

fun convertir(millas:Double = 0.0):Double = millas * 1852