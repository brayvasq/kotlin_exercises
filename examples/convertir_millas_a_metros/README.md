### Convertir de millas a metros

Diseñar el algoritmo (ordinograma y pseudocódigo) correspondiente a un programa 
que lea el valor correspondiente a una distancia en millas marinas y las escriba
expresadas en metros. Sabiendo que 1 milla marina equivale a 1852 metro