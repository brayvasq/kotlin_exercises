package punto3

import java.io.File
import kotlin.concurrent.thread

/**
 * Created by brayan on 8/02/17.
 */

var narchivos : Int = 0;
var npalabras : Int = 0;
var nlineas   : Int = 0;
var nletras   : Int = 0;
fun main(args: Array<String>){
    println("Hola");

    println("secuencial ->\n")
    var num1 = System.currentTimeMillis()
    leer("resources/fichero1kotlin.txt")
    leer("resources/fichero2kotlin.txt")
    leer("resources/fichero2kotlin.txt")
    var num2 = System.currentTimeMillis()
    println(num2-num1)

    narchivos = 0
    nlineas = 0
    npalabras = 0
    nletras = 0
    println("con hilos -> \n")
    num1 = System.currentTimeMillis()
    leerhilo("resources/fichero1kotlin.txt")
    leerhilo("resources/fichero2kotlin.txt")
    leerhilo("resources/fichero2kotlin.txt")
    num2 = System.currentTimeMillis()
    println(num2-num1)
}


fun leer(ruta: String){
    //println("entro al run")

    var archivo = File(ruta);
    var contenido = archivo.readLines();
    narchivos++;
    //println("########### " + punto3.narchivos)
    var palabras : Int = 0;
    var letras : Int = 0;
    //println("Archivo : "+ narchivos);
    var lineas = contenido.count()
    //println("Lineas : "+lineas)

    contenido.forEach {
        for(i in 0..it.length-1){
            val pal=it.get(i).toString()
            //println(i.toString()+" - "+pal)
            letras++
            if(pal.equals(" ")){
                palabras++
            }
        }
    }
    //println("Palabras : "+palabras)
    //println("Letras : "+letras)

    npalabras += palabras
    nletras += letras
    nlineas += lineas

    synchronized(System.out) {
        println("Archivos : " + narchivos + " lineas : " + nlineas + " palabras : " + npalabras + " letras : " + nletras)
    }
}


fun leerhilo(ruta : String){
    thread {
        leer(ruta)
    }
    /*var hilo = Runnable(){
        fun run(){
            println("llamo al leer")
            leer(ruta)
        }
    }

    hilo.run()*/

}