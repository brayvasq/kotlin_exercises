package punto4

import java.util.*
import kotlin.concurrent.thread

/**
 * Created by brayan on 11/02/17.
 */

//val inicio : Int = 1
//val final : Int = 70
var tortuga : Int = 1
var liebre : Int = 1
var fin : Boolean = false;
var GL : Boolean = false
var GT : Boolean = false

//var prob10 = "Resbalon grande"
//var prob20 = arrayOf("Duerme","Resbala","Gran Salto","Resbalon peque")
//var prob30 = arrayOf("peque salto","avance lento")
//var prob50 = "avance rapido"
fun main(args: Array<String>){

    correr("T")
    correr("L")

}

fun avanzar(competidor:String,avance: Int){
    if (competidor.equals("T")){
        tortuga += avance
        if(tortuga < 1){
            tortuga = 1
        }else if(tortuga >= 70){
            tortuga = 70
            GT = true
        }

    }else if(competidor.equals("L")){
        liebre += avance
        if(liebre < 1){
            liebre = 1
        }else if(liebre >= 70){
            liebre = 70
            GL = true
        }
    }
}

fun simular(competidor: String){
    while(!fin){
        Thread.sleep(1000)

        var num = Random()
        var tam = num.nextInt(100)+1
        var accion =""
        if(competidor.equals("T")){
            if(tam <= 50){
                avanzar(competidor,3)
                accion = "avance rapido +3"
            }else if(tam>50 && tam<=70){
                avanzar(competidor,-6)
                accion = "Resbaló -6"
            }else if(tam>70 && tam<=100){
                avanzar(competidor,1)
                accion = "avance lento +1"
            }
        }else if(competidor.equals("L")){
            if(tam<=20){
                accion = "Duerme"
            }else if(tam>20 && tam<=40){
                avanzar(competidor,9)
                accion = "gran salto +9"
            }else if(tam>40 && tam<=50){
                avanzar(competidor,-12)
                accion = "resbalón grande -12"
            }else if(tam>50 && tam<=80){
                avanzar(competidor,1)
                accion = "pequeño salto +1"
            }else if(tam>80 && tam <=100){
                avanzar(competidor,-2)
                accion = "resbalon pequeño -2"
            }
        }
        if(competidor.equals("T")) {
            println(competidor + " - pos : "+ tortuga+" - acción : "+accion)
        }else{
            println(competidor + " - pos : "+ liebre+" - acción : "+accion)
        }
        if(GL && GT){
            println("empate")
            fin = true
        }else if(GL){
            println("Gana L")
            fin = true
        }else if(GT){
            println("Gana T")
            fin = true
        }
    }
}

fun correr(competidor: String){
    thread{
        simular(competidor)
    }
}