package punto1;

/**
 * Created by cvem8165 on 6/02/17.
 */
public class punto1 {
    public static void main(String[] args) {
        // write your code here



        Thread hilo1 = new Thread(() -> {
            for (int i = 1; i <= 60; i++) {
                try {
                    Thread.sleep(1000);
                }
                catch (Exception e){
                    System.out.println(e);
                }
                synchronized (System.out){
                    System.out.println("Tiempo de espera " + (1*i)
                            + "\n Nombre = A");
                }
            }

        });

        Thread hilo2 = new Thread(() -> {
            for (int i = 1; i <= 30; i++) {
                try {
                    Thread.sleep(2000);
                }
                catch (Exception e){
                    System.out.println(e);
                }
                synchronized (System.out){
                    System.out.println("Tiempo de espera " + (2*i)
                            + "\n Nombre = B");
                }
            }

        });

        Thread hilo3 = new Thread(() -> {
            for (int i = 1; i <= 20; i++) {
                try {
                    Thread.sleep(3000);
                }
                catch (Exception e){
                    System.out.println(e);
                }
                synchronized (System.out){
                    System.out.println("Tiempo de espera " + (3*i)
                            + "\n Nombre = C");
                }
            }

        });

        Thread hilo4 = new Thread(() -> {
            for (int i = 1; i <= 15;i++) {
                try {
                    Thread.sleep(4000);
                }
                catch (Exception e){
                    System.out.println(e);
                }
                synchronized (System.out){
                    System.out.println("Tiempo de espera " + (4*i)
                            + "\n Nombre = D");
                }
            }

        });

        Thread hilo5 = new Thread(() -> {
            for (int i = 1; i <= 12;i++) {
                try {
                    Thread.sleep(5000);
                }
                catch (Exception e){
                    System.out.println(e);
                }
                synchronized (System.out){
                    System.out.println("Tiempo de espera " + (5*i)
                            + "\n Nombre = E");
                }
            }

        });

        Thread hilo6 = new Thread(() -> {
            for (int i = 1; i <= 10;i++) {
                try {
                    Thread.sleep(6000);
                }
                catch (Exception e){
                    System.out.println(e);
                }
                synchronized (System.out){
                    System.out.println("Tiempo de espera " + (6*i)
                            + "\n Nombre = F");
                }
            }

        });


        hilo1.start();
        hilo2.start();
        hilo3.start();
        hilo4.start();
        hilo5.start();
        hilo6.start();

    }

}
