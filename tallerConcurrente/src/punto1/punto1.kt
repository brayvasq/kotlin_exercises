package punto1

/**
 * Created by cvem8165 on 6/02/17.
 */


fun main(args: Array<String>) {
    // write your code here


    val hilo1 = Thread {
        for (i in 0..59) {
            try {
                Thread.sleep(1000)
            } catch (e: Exception) {
                println(e)
            }

            synchronized(System.out) {
                println("Tiempo de espera" + 1 * i
                        + "\n Nombre = A")
            }
        }

    }

    val hilo2 = Thread {
        for (i in 0..29) {
            try {
                Thread.sleep(2000)
            } catch (e: Exception) {
                println(e)
            }

            synchronized(System.out) {
                println("Tiempo de espera" + 2 * i
                        + "\n Nombre = B")
            }
        }

    }

    val hilo3 = Thread {
        for (i in 0..19) {
            try {
                Thread.sleep(3000)
            } catch (e: Exception) {
                println(e)
            }

            synchronized(System.out) {
                println("Tiempo de espera" + 3 * i
                        + "\n Nombre = C")
            }
        }

    }

    val hilo4 = Thread {
        for (i in 0..14) {
            try {
                Thread.sleep(4000)
            } catch (e: Exception) {
                println(e)
            }

            synchronized(System.out) {
                println("Tiempo de espera" + 4 * i
                        + "\n Nombre = D")
            }
        }

    }

    val hilo5 = Thread {
        for (i in 0..11) {
            try {
                Thread.sleep(5000)
            } catch (e: Exception) {
                println(e)
            }

            synchronized(System.out) {
                println("Tiempo de espera" + (5 * i)
                        + "\n Nombre = E")
            }
        }

    }

    val hilo6 = Thread {
        for (i in 0..9) {
            try {
                Thread.sleep(6000)
            } catch (e: Exception) {
                println(e)
            }

            synchronized(System.out) {
                println("Tiempo de espera" + (6 * i)
                        + "\n Nombre = F")
            }
        }

    }


    hilo1.start()
    hilo2.start()
    hilo3.start()
    hilo4.start()
    hilo5.start()
    hilo6.start()


}


