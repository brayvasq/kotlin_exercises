package Punto5

/**
 * Created by cvem8165 on 11/02/17.
 */
fun main(args: Array<String>) {

    val tamano = 1000

    val matriz_1_100 = Array(tamano, { Array(tamano, { (Math.random() * 99 + 1).toInt() }) })
    val matriz_2_100 = Array(tamano, { Array(tamano, { (Math.random() * 99 + 1).toInt() }) })

    //var matriz_1 = arrayOf(intArrayOf(2,0,1), intArrayOf(3,0,0), intArrayOf(5,1,1))
    //var matriz_2 = arrayOf(intArrayOf(1,0,1), intArrayOf(1,2,1), intArrayOf(1,1,0))

    val inicio = System.currentTimeMillis()
    multiplicarMatricesConcurrente(matriz_1_100, matriz_2_100, tamano)
    print("Tiempo total " + (System.currentTimeMillis() - inicio))


}

fun multiplicarMatrices(array1: Array<Array<Int>>, array2: Array<Array<Int>>, tamano: Int) {
    val matriz_respuesta = Array(tamano, { Array(tamano, { 1 }) })

    //http://www.vitutor.com/algebra/matrices/producto.html

    for (i in 0..array1.size - 1) {
        for (j in 0..array1[0].size - 1) {
            var respuesta = 0
            for (k in 0..array2[0].size - 1) {
                respuesta += array1[i][k] * array2[k][j]
            }
            matriz_respuesta[i][j] = respuesta
        }
    }

    /*for (i in 0..matriz_respuesta.size - 1){
        for (j in 0..matriz_respuesta[0].size -1){
            print(matriz_respuesta[i][j].toString() + " ")
        }
        println()
    }
    */
}

fun multiplicarMatricesConcurrente(array1: Array<Array<Int>>, array2: Array<Array<Int>>, tamano: Int) {

    val matriz_respuesta = Array(tamano, { Array(tamano, { 0 }) })

    val hilo1: Thread
    val hilo2: Thread
    val hilo3: Thread
    val hilo4: Thread
    when (tamano) {
        100 -> {
            hilo1 = Thread {
                for (i in 0..49) {
                    for (j in 0..49) {
                        var respuesta = 0
                        for (k in 0..49) {
                            respuesta += array1[i][k] * array2[k][j]
                        }
                        matriz_respuesta[i][j] = respuesta
                    }
                }
                for (i in 0..49) {
                    for (j in 0..49) {
                        var respuesta = 0
                        for (k in 50..99) {
                            respuesta += array1[i][k] * array2[k][j]
                        }
                        matriz_respuesta[i][j] += respuesta
                    }
                }

            }
            hilo2 = Thread {
                for (i in 0..49) {
                    for (j in 50..99) {
                        var respuesta = 0
                        for (k in 0..49) {
                            respuesta += array1[i][k] * array2[k][j]
                        }
                        matriz_respuesta[i][j] = respuesta
                    }
                }
                for (i in 0..49) {
                    for (j in 50..99) {
                        var respuesta = 0
                        for (k in 50..99) {
                            respuesta += array1[i][k] * array2[k][j]
                        }
                        matriz_respuesta[i][j] += respuesta
                    }
                }

            }
            hilo3 = Thread {
                for (i in 50..99) {
                    for (j in 0..49) {
                        var respuesta = 0
                        for (k in 0..49) {
                            respuesta += array1[i][k] * array2[k][j]
                        }
                        matriz_respuesta[i][j] = respuesta
                    }
                }
                for (i in 50..99) {
                    for (j in 0..49) {
                        var respuesta = 0
                        for (k in 50..99) {
                            respuesta += array1[i][k] * array2[k][j]
                        }
                        matriz_respuesta[i][j] += respuesta
                    }
                }

            }
            hilo4 = Thread {
                for (i in 50..99) {
                    for (j in 50..99) {
                        var respuesta = 0
                        for (k in 0..49) {
                            respuesta += array1[i][k] * array2[k][j]
                        }
                        matriz_respuesta[i][j] = respuesta
                    }
                }
                for (i in 50..99) {
                    for (j in 50..99) {
                        var respuesta = 0
                        for (k in 50..99) {
                            respuesta += array1[i][k] * array2[k][j]
                        }
                        matriz_respuesta[i][j] += respuesta
                    }
                }

            }
        }
        500 -> {
            hilo1 = Thread {
                for (i in 0..249) {
                    for (j in 0..249) {
                        var respuesta = 0
                        for (k in 0..249) {
                            respuesta += array1[i][k] * array2[k][j]
                        }
                        matriz_respuesta[i][j] = respuesta
                    }
                }
                for (i in 0..249) {
                    for (j in 0..249) {
                        var respuesta = 0
                        for (k in 250..499) {
                            respuesta += array1[i][k] * array2[k][j]
                        }
                        matriz_respuesta[i][j] += respuesta
                    }
                }

            }
            hilo2 = Thread {
                for (i in 0..249) {
                    for (j in 249..499) {
                        var respuesta = 0
                        for (k in 0..249) {
                            respuesta += array1[i][k] * array2[k][j]
                        }
                        matriz_respuesta[i][j] = respuesta
                    }
                }
                for (i in 0..249) {
                    for (j in 249..499) {
                        var respuesta = 0
                        for (k in 250..499) {
                            respuesta += array1[i][k] * array2[k][j]
                        }
                        matriz_respuesta[i][j] += respuesta
                    }
                }

            }
            hilo3 = Thread {
                for (i in 250..499) {
                    for (j in 0..249) {
                        var respuesta = 0
                        for (k in 0..249) {
                            respuesta += array1[i][k] * array2[k][j]
                        }
                        matriz_respuesta[i][j] = respuesta
                    }
                }
                for (i in 250..499) {
                    for (j in 0..249) {
                        var respuesta = 0
                        for (k in 250..499) {
                            respuesta += array1[i][k] * array2[k][j]
                        }
                        matriz_respuesta[i][j] += respuesta
                    }
                }

            }
            hilo4 = Thread {
                for (i in 250..499) {
                    for (j in 250..499) {
                        var respuesta = 0
                        for (k in 0..249) {
                            respuesta += array1[i][k] * array2[k][j]
                        }
                        matriz_respuesta[i][j] = respuesta
                    }
                }
                for (i in 250..99) {
                    for (j in 250..499) {
                        var respuesta = 0
                        for (k in 250..499) {
                            respuesta += array1[i][k] * array2[k][j]
                        }
                        matriz_respuesta[i][j] += respuesta
                    }
                }

            }

        }
        else->{
            hilo1 = Thread {
                for (i in 0..499) {
                    for (j in 0..499) {
                        var respuesta = 0
                        for (k in 0..499) {
                            respuesta += array1[i][k] * array2[k][j]
                        }
                        matriz_respuesta[i][j] = respuesta
                    }
                }
                for (i in 0..499) {
                    for (j in 0..499) {
                        var respuesta = 0
                        for (k in 500..999) {
                            respuesta += array1[i][k] * array2[k][j]
                        }
                        matriz_respuesta[i][j] += respuesta
                    }
                }

            }
            hilo2 = Thread {
                for (i in 0..499) {
                    for (j in 500..999) {
                        var respuesta = 0
                        for (k in 0..499) {
                            respuesta += array1[i][k] * array2[k][j]
                        }
                        matriz_respuesta[i][j] = respuesta
                    }
                }
                for (i in 0..499) {
                    for (j in 500..999) {
                        var respuesta = 0
                        for (k in 500..999) {
                            respuesta += array1[i][k] * array2[k][j]
                        }
                        matriz_respuesta[i][j] += respuesta
                    }
                }

            }
            hilo3 = Thread {
                for (i in 500..999) {
                    for (j in 0..499) {
                        var respuesta = 0
                        for (k in 0..499) {
                            respuesta += array1[i][k] * array2[k][j]
                        }
                        matriz_respuesta[i][j] = respuesta
                    }
                }
                for (i in 500..999) {
                    for (j in 0..499) {
                        var respuesta = 0
                        for (k in 500..999) {
                            respuesta += array1[i][k] * array2[k][j]
                        }
                        matriz_respuesta[i][j] += respuesta
                    }
                }

            }
            hilo4 = Thread {
                for (i in 500..999) {
                    for (j in 500..999) {
                        var respuesta = 0
                        for (k in 0..499) {
                            respuesta += array1[i][k] * array2[k][j]
                        }
                        matriz_respuesta[i][j] = respuesta
                    }
                }
                for (i in 500..999) {
                    for (j in 500..999) {
                        var respuesta = 0
                        for (k in 500..999) {
                            respuesta += array1[i][k] * array2[k][j]
                        }
                        matriz_respuesta[i][j] += respuesta
                    }
                }

            }
        }

    }
    hilo1.start()
    hilo2.start()
    hilo3.start()
    hilo4.start()


}

