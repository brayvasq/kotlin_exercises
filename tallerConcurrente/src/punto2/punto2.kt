package punto2

/**
 * Created by cvem8165 on 6/02/17.
 */

var contador = 0 // var para variables, val para constantes

fun main(args: Array<String>) {


    val runnableNoSync = Runnable {
        for (i in 1..5000) {
            synchronized(System.out) {
                contador = contador + 1
                println(contador)
            }

        }

    }
    val runnableSync = Runnable {
        for (i in 1..5000) {
            synchronized(contador) {
                synchronized(System.out) {
                    contador = contador + 1
                    println(contador)
                }
            }
        }

    }

    correr(runnableNoSync)
    correr(runnableSync)

}

fun correr(runnable: Runnable) {

    val hilo1 = Thread(runnable)
    val hilo2 = Thread(runnable)
    val hilo3 = Thread(runnable)
    val hilo4 = Thread(runnable)

    hilo1.start()
    hilo2.start()
    hilo3.start()
    hilo4.start()

}